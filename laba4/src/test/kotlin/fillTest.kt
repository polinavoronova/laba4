package org.fillmatrix

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class TestFillMatrix {
    @Test
    fun testSize0() {
        val matrix = mock(Matrix::class.java)
        try{
            fillMatrix(matrix, 0)
        } catch (ex: IllegalArgumentException){
            print(ex.message)
        }
    }
    @Test
    fun testSize() {
        val matrix = mock(Matrix::class.java)
        try{
            fillMatrix(matrix, -1)
        } catch (ex: IllegalArgumentException){
            print(ex.message)
        }
    }
    @Test
    fun testSize1() {
        val matrix = mock(Matrix::class.java)
        fillMatrix(matrix, 1)

        Mockito.verify(matrix).set(0, 0, 1)
    }
    @Test
    fun testSize5() {
        val matrix = mock(Matrix::class.java)
        fillMatrix(matrix, 5)

        Mockito.verify(matrix).set(1, 0, 1)
        Mockito.verify(matrix).set(2, 0, 2)
        Mockito.verify(matrix).set(3, 0, 3)
        Mockito.verify(matrix).set(4, 0, 4)
        Mockito.verify(matrix).set(4, 1, 5)
        Mockito.verify(matrix).set(3, 1, 6)
        Mockito.verify(matrix).set(2, 1, 7)
        Mockito.verify(matrix).set(3, 2, 8)
        Mockito.verify(matrix).set(4, 2, 9)
        Mockito.verify(matrix).set(4, 3, 10)
        Mockito.verify(matrix).set(4, 4, 11)
        Mockito.verify(matrix).set(3, 3, 12)
        Mockito.verify(matrix).set(2, 2, 13)
        Mockito.verify(matrix).set(1, 1, 14)
        Mockito.verify(matrix).set(0, 0, 15)
        Mockito.verify(matrix).set(0, 1, 16)
        Mockito.verify(matrix).set(1, 2, 17)
        Mockito.verify(matrix).set(2, 3, 18)
        Mockito.verify(matrix).set(3, 4, 19)
        Mockito.verify(matrix).set(2, 4, 20)
        Mockito.verify(matrix).set(1, 3, 21)
        Mockito.verify(matrix).set(0, 2, 22)
        Mockito.verify(matrix).set(0, 3, 23)
        Mockito.verify(matrix).set(1, 4, 24)
        Mockito.verify(matrix).set(0, 4, 25)
    }
    @Test
    fun testSize4() {
        val matrix = mock(Matrix::class.java)
        fillMatrix(matrix, 4)

        Mockito.verify(matrix).set(1, 0, 1)
        Mockito.verify(matrix).set(2, 0, 2)
        Mockito.verify(matrix).set(3, 0, 3)
        Mockito.verify(matrix).set(3, 1, 4)
        Mockito.verify(matrix).set(2, 1, 5)
        Mockito.verify(matrix).set(3, 2, 6)
        Mockito.verify(matrix).set(3, 3, 7)
        Mockito.verify(matrix).set(2, 2, 8)
        Mockito.verify(matrix).set(1, 1, 9)
        Mockito.verify(matrix).set(0, 0, 10)
        Mockito.verify(matrix).set(0, 1, 11)
        Mockito.verify(matrix).set(1, 2, 12)
        Mockito.verify(matrix).set(2, 3, 13)
        Mockito.verify(matrix).set(1, 3, 14)
        Mockito.verify(matrix).set(0, 2, 15)
        Mockito.verify(matrix).set(0, 3, 16)
}
}
