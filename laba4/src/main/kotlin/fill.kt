package org.fillmatrix

import java.lang.IllegalArgumentException


class Builder(_matrix: Matrix, _size: Int) {
    var size: Int
    var matrix: Matrix

    var stage: Int = 0
    var x: Int = 0
    var y: Int = 1

    val UP_LEFT = Pair(-1, -1)
    val UP = Pair(0, -1)
    val UP_RIGHT = Pair(1, -1)
    val RIGHT = Pair(1, 0)
    val DOWN_RIGHT = Pair(1, 1)
    val DOWN = Pair(0, 1)
    val DOWN_LEFT = Pair(-1, 1)
    val LEFT = Pair(-1, 0)

    var main_direction: Pair<Int, Int> = DOWN
    var local_direction: Pair<Int, Int> = DOWN_RIGHT

    /**
     *INITIALIZATION
     */
    init {
        matrix = _matrix
        size = _size
        if (size == 1) {
            y = 0
        }
    }

    /**
     *Filling the matrix
     */
    fun build() {
        IntRange(1, size * size).forEach{
            matrix.set(y, x, it)
            step()
        }
    }

    /**
     *Move position for next value
     */
    fun step() {
        if (stage == 0) { // под главной диагональю
            if (y == size - 1 && x >= size - 2) {
                if (x == size - 1) { // переход к главной диагонали, второму алгоритму
                    stage = 1
                }
                main_direction = UP_LEFT
                local_direction = RIGHT
            }
            else if (local_direction == RIGHT || local_direction == DOWN_RIGHT) {
                local_direction = main_direction
            }
            else if ((y == size - 1 && local_direction == DOWN) || (x == y - 1 && local_direction == UP)) { // дошли до низа или главной диагонали
                if (x == y - 1) {
                    local_direction = DOWN_RIGHT
                }
                else {
                    local_direction = RIGHT
                }

                main_direction = Pair(-main_direction.first, -main_direction.second)
            }
        }

        if (stage == 1) { // главная диагональ и выше
            if (local_direction == RIGHT || local_direction == UP) {
                local_direction = main_direction
            }
            else if ((y == 0 && local_direction == UP_LEFT) || (x == size - 1 && local_direction == DOWN_RIGHT)) {
                if (y == 0) { // на диагонали
                    local_direction = RIGHT
                }
                else{
                    local_direction = UP
                }

                main_direction = Pair(-main_direction.first, -main_direction.second)
            }
        }
        x += local_direction.first
        y += local_direction.second
    }
}


fun fillMatrix(matrix: Matrix, size: Int) {
    if (size <= 0){
        throw IllegalArgumentException("Size shouldn't be lower or equal 0")
    }
    val builder = Builder(matrix, size)
    builder.build()
}
