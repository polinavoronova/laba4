package org.fillmatrix

/**
 *  Interface for any object as matrix
 */
interface Matrix {
    fun set(row: Int, column: Int, value: Int)
}